package com.epam.tat.lesson7.browser;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public enum BrowserType {
    REMOTE("firefox*"),
    FIREFOX("firefox"),
    HTMLUNIT("htmlunit"),
    CHROME("chrome");

    private String alias;

    BrowserType(String alias) {
        this.alias = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for(BrowserType type: BrowserType.values()){
            if(type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}
