package com.epam.tat.lesson7.cli;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.Messages;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class CredentialHandler extends OptionHandler<Credentials> {

    public CredentialHandler(CmdLineParser parser, OptionDef option, Setter<? super Credentials> setter) {
        super(parser, option, setter);
    }

    @Override
    public int parseArguments(Parameters parameters) throws CmdLineException {
        if (this.option.isArgument()) {
            String valueStr = parameters.getParameter(0).toLowerCase();
            if (valueStr == null || valueStr.isEmpty()) {
                throw new CmdLineException(this.owner, Messages.ILLEGAL_CHAR, new String[]{valueStr});
            } else {
                String[] parts = valueStr.split(":");
                if (parts.length < 2)
                    throw new CmdLineException(this.owner, Messages.ILLEGAL_CHAR, new String[]{valueStr});

                Credentials credentials = new Credentials(parts[0], parts[1]);
                this.setter.addValue(credentials);
                return 1;
            }
        } else {
            this.setter.addValue(null);
            return 0;
        }
    }

    @Override
    public String getDefaultMetaVariable() {
        return null;
    }
}
