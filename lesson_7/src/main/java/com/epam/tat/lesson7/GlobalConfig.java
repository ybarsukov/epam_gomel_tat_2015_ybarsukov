package com.epam.tat.lesson7;

import com.epam.tat.lesson7.browser.BrowserType;
import com.epam.tat.lesson7.cli.TestRunnerOptions;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class GlobalConfig {

    private static BrowserType browserType = BrowserType.HTMLUNIT;

    private static ParallelMode parallelMode = ParallelMode.FALSE;

    private static int threadCount = 1;

    public static void updateFromOptions(TestRunnerOptions options) {
        browserType = BrowserType.getTypeByAlias(options.browserType);
        parallelMode = ParallelMode.getTypeByAlias(options.parallelMode);
        threadCount = options.threadCount;
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static void setBrowserType(BrowserType browserType) {
        GlobalConfig.browserType = browserType;
    }

    public static ParallelMode getParallelMode() {
        return parallelMode;
    }

    public static void setParallelMode(ParallelMode parallelMode) {
        GlobalConfig.parallelMode = parallelMode;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static void setThreadCount(int threadCount) {
        GlobalConfig.threadCount = threadCount;
    }
}
