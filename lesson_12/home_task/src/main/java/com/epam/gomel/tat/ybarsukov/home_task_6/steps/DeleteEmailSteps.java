package com.epam.gomel.tat.ybarsukov.home_task_6.steps;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.Account;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.User;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.LoginGuiService;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.MailGuiService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

/**
 * Created by ybarsukov on 12.04.2015.
 */
public class DeleteEmailSteps {

    private User user = new User();
    private Letter letter;
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();

    @Given("User has the letter in inbox list")
    public void userHasLetterInInboxList() {
        Account account = user.createAccount(AccountBuilder.getDefaultAccount());
        loginGuiService.loginToAccountMailbox(account);
        letter = user.createLetter(LetterBuilder.getDefaultLetter());
        mailGuiService.sendEmail(letter);
        Assert.assertTrue(mailGuiService.isEmailInInboxList(letter));
    }

    @When("User mark letter as deleted")
    public void userMarkLetterAsDeleted() {
        mailGuiService.deleteEmail(letter);
    }

    @Then("Letter appears in deleted list")
    public void checkLetterInDeletedList() {
        Assert.assertTrue(mailGuiService.isEmailInDeletedList(letter));
    }

    @Given("User has letter in deleted list")
    public void userHasLetterInDeletedList() {
        userHasLetterInInboxList();
        userMarkLetterAsDeleted();
        checkLetterInDeletedList();
    }

    @When("User mark letter as deleted forever")
    public void userMarkLetterAsDeletedForever() {
        mailGuiService.deleteEmailForever(letter);
    }

    @Then("Letter has deleted forever")
    public void checkLetterDeletedForever() {
        Assert.assertTrue(mailGuiService.isEmailAbsentOnDeletedList(letter));
    }
}
