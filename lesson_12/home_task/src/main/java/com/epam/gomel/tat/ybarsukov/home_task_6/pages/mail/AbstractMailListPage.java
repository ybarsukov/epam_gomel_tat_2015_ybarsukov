package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 22.03.2015.
 */
public abstract class AbstractMailListPage {

    private Element emailLink;
    private Element emailCheckbox;
    private Element deleteEmailButton = new Element(By.xpath("//div[@class = 'b-toolbar__i']//a[@data-action = 'delete']"));
    private Element toSpamEmailButton = new Element(By.xpath("//div[@class = 'b-toolbar__i']//a[@data-action = 'tospam']"));
    private Element toNotSpamEmailButton = new Element(By.xpath("//div[@class = 'b-toolbar__i']//a[@data-action = 'notspam']"));

    protected AbstractMailListPage(Element emailLink, Element emailCheckbox) {
        this.emailLink = emailLink;
        this.emailCheckbox = emailCheckbox;
    }

    public boolean isMessagePresent(String subject) {
        emailLink.setPattern(subject);
        return emailLink.isElementOnPage();
    }

    public boolean isMessageAbsent(String subject) {
        emailLink.setPattern(subject);
        return !emailLink.isElementPresent();
    }

    public LetterContentPage openLetter(String subject) {
        emailLink.setPattern(subject);
        emailLink.click();
        return new LetterContentPage();
    }

    public void deleteEmail(String subject) {
        markEmail(subject, deleteEmailButton);
    }

    public void markEmailAsSpam(String subject) {
        markEmail(subject, toSpamEmailButton);
    }

    public void markEmailAsNotSpam(String subject) {
        markEmail(subject, toNotSpamEmailButton);
    }

    private void markEmail(String subject, Element element) {
        emailCheckbox.setPattern(subject);
        emailCheckbox.twoClick(element);
    }
}
