package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;

/**
 * Created by ybarsukov on 19.03.2015.
 */
public class SpamMailListPage extends AbstractMailListPage {

    private static Element emailLink = new Element("//label[text() = 'Спам']/ancestor::div[@class = 'block-messages']//span[@class = 'b-messages__subject'][text()='%s']");
    private static Element emailCheckbox = new Element("//label[text() = 'Спам']/ancestor::div[@class = 'block-messages']//span[@class = 'b-messages__subject'][text()='%s']/ancestor::div[1]//input[@type = 'checkbox']");

    public SpamMailListPage() {
        super(emailLink, emailCheckbox);
    }
}
