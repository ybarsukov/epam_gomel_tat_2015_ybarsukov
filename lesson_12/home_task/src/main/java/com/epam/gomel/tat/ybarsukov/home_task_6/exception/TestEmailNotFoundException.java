package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 22.03.2015.
 */
public class TestEmailNotFoundException extends TestCommonRuntimeException {
    public TestEmailNotFoundException(String s) {
        super(s);
    }

    public TestEmailNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
