package com.epam.gomel.tat.ybarsukov.home_task_6.runner;

import com.epam.gomel.tat.ybarsukov.home_task_6.steps.DeleteEmailSteps;
import com.epam.gomel.tat.ybarsukov.home_task_6.steps.LoginToMailSteps;
import com.epam.gomel.tat.ybarsukov.home_task_6.steps.SpamEmailSteps;
import com.epam.gomel.tat.ybarsukov.home_task_6.steps.SendEmailSteps;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ybarsukov on 09.04.2015.
 */

public class JBehaveRunner extends JUnitStories {

    public static final String LOGIN_STORY = "stories/login_mail.story";
    public static final String SEND_EMAIL_STORY = "stories/send_email.story";
    public static final String SPAM_EMAIL_STORY = "stories/spam_email.story";
    public static final String DELETE_EMAIL_STORY = "stories/delete_email.story";

    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration()
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new LoginToMailSteps(), new SendEmailSteps(), new SpamEmailSteps(), new DeleteEmailSteps());
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()).getFile(),
                Arrays.asList(LOGIN_STORY, SEND_EMAIL_STORY, SPAM_EMAIL_STORY, DELETE_EMAIL_STORY), null);
    }
}
