package com.epam.gomel.tat.ybarsukov.home_task_6.global;

import com.epam.gomel.tat.ybarsukov.home_task_6.reporting.Logger;
import com.epam.gomel.tat.ybarsukov.home_task_6.utils.FileTools;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ybarsukov on 25.03.2015.
 */
public class Options {

    @Option(name = "-bt", usage = "browser type", required = true)
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-host", usage = "remote host")
    private String remoteHost = "localhost";

    @Option(name = "-port", usage = "remote port")
    private String remotePort = "4444";

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", usage = "thread count")
    private int threadCount = 1;

    @Option(name = "-suites", required = true, handler = StringArrayOptionHandler.class)
    private List<String> suitesFilePath = new ArrayList<String>();

    @Option(name = "-driver", usage = "chromedriver")
    private String chromeDriver = FileTools.getResource("drivers/chromedriver.exe");

    private static Options instance = null;

    private Options() {
    }

    public static Options getInstance() {
        Logger.info("Get instance: " + Options.class.getSimpleName());
        if (instance == null) {
            instance = new Options();
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getParallelMode() {
        return parallelMode.name().toLowerCase();
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public String getRemotePort() {
        return remotePort;
    }

    public List<String> getSuitesFilePath() {
        return suitesFilePath;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    @Override
    public String toString() {
        return "Options{" +
                "browserType='" + browserType.name() + '\'' +
                ", remoteHost='" + remoteHost + '\'' +
                ", remotePort='" + remotePort + '\'' +
                ", parallelMode='" + parallelMode.name() + '\'' +
                ", threadCount='" + threadCount + '\'' +
                ", chromeDriver='" + chromeDriver + '\'' +
                ", suitesFilePath=" + suitesFilePath +
                '}';
    }
}
