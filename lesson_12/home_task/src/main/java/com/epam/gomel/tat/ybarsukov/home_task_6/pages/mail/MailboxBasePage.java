package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 19.03.2015.
 */
public class MailboxBasePage {

    private Element yandexMailboxUrl = new Element("http://mail.yandex.ru");
    private Element inboxButton = new Element(By.xpath("//div[@class = 'b-folders__i']//a[@href = '#inbox']"));
    private Element sentButton = new Element(By.xpath("//div[@class = 'b-folders__i']//a[@href = '#sent']"));
    private Element deletedButton = new Element(By.xpath("//div[@class = 'b-folders__i']//a[@href = '#trash']"));
    private Element spamButton = new Element(By.xpath("//div[@class = 'b-folders__i']//a[@href = '#spam']"));
    private Element draftsButton = new Element(By.xpath("//div[@class = 'b-folders__i']//a[@href = '#draft']"));
    private Element validateEmailLink = new Element(By.xpath("//a[@id='nb-1']/span[1]"));

    public MailboxBasePage open() {
        yandexMailboxUrl.open();
        return this;
    }

    public InboxMailListPage openInboxPage() {
        inboxButton.click();
        return new InboxMailListPage();
    }

    public SentMailListPage openSentPage() {
        sentButton.click();
        return new SentMailListPage();
    }

    public DeletedMailListPage openDeletedPage() {
        deletedButton.click();
        return new DeletedMailListPage();
    }

    public SpamMailListPage openSpamPage() {
        spamButton.click();
        return new SpamMailListPage();
    }

    public DraftsMailListPage openDraftsPage() {
        draftsButton.click();
        return new DraftsMailListPage();
    }

    public String getUserEmail() {
        return validateEmailLink.getElement().getText();
    }
}

