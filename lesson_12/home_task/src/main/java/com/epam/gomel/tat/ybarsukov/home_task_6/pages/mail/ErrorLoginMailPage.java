package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 22.03.2015.
 */
public class ErrorLoginMailPage {

    private Element errorMessage = new Element(By.cssSelector("div.error-msg"));
    public static final String INCORRECT_PASSWORD_MESSAGE = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public String getErrorMessage() {
        return errorMessage.getElement().getText();
    }
}
