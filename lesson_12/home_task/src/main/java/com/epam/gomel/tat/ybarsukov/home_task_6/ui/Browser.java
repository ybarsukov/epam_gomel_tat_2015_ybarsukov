package com.epam.gomel.tat.ybarsukov.home_task_6.ui;

import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestUnsupportedBrowserException;
import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestWrongUrlException;
import com.epam.gomel.tat.ybarsukov.home_task_6.global.BrowserType;
import com.epam.gomel.tat.ybarsukov.home_task_6.global.Options;
import com.epam.gomel.tat.ybarsukov.home_task_6.reporting.Logger;
import com.epam.gomel.tat.ybarsukov.home_task_6.utils.Timeout;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by ybarsukov on 18.03.2015.
 */
public class Browser {
    private static final int WAIT_ELEMENT_TIMEOUT = 20;
    private static final int AJAX_TIMEOUT = 20;
    private static final int WAIT_FOR_DOWNLOAD_TIMEOUT = 5;
    private static final int PAGE_LOAD_TIMEOUT_SECONDS = 20;
    private static final int IMPLICITLY_WAIT_TIMEOUT_SECONDS = 0;
    private static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private static BrowserType browserType;
    private WebDriver driver;
    private boolean augmented;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();

    public static synchronized Browser getInstance() {
        Logger.info("Get instance: " + Browser.class.getSimpleName());
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        Logger.info("Init browser");
        browserType = Options.getInstance().getBrowserType();
        Logger.info("Browser type is: " + browserType.name());
        WebDriver driver;

        switch (browserType) {
            case FIREFOX: {
                driver = new FirefoxDriver(getFirefoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
            }
            case FIREFOXR: {
                try {
                    driver = new RemoteWebDriver(new URL("http://" + Options.getInstance().getRemoteHost() + ":" + Options.getInstance().getRemotePort() + "/wd/hub"),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new TestWrongUrlException("Wrong remote URL!");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
            }
            case CHROME: {
                driver = new ChromeDriver(getChromeProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
            }
            case CHROMER: {
                try {
                    //java -jar selenium-server-standalone-2.45.0.jar -Dwebdriver.chrome.driver="chromedriver.exe"
                    driver = new RemoteWebDriver(new URL("http://" + Options.getInstance().getRemoteHost() + ":" + Options.getInstance().getRemotePort() + "/wd/hub"),
                            DesiredCapabilities.chrome());
                } catch (MalformedURLException e) {
                    throw new TestWrongUrlException("Wrong remote URL!");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
            }
            case HTMLUNIT: {
                driver = new HtmlUnitDriver(DesiredCapabilities.htmlUnit());
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
            }
            default: {
                Logger.error(browserType.name() + " Is unsupported browser!");
                throw new TestUnsupportedBrowserException(browserType.name() + " Is unsupported browser!");
            }
        }
        return new Browser(driver);
    }

    private static FirefoxProfile getFirefoxProfile() {
        Logger.info("Get profile: " + browserType.name());
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", FileUtils.getTempDirectoryPath());
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        return profile;
    }

    private static DesiredCapabilities getChromeProfile() {
        Logger.info("Get profile: " + browserType.name());
        System.setProperty("webdriver.chrome.driver", Options.getInstance().getChromeDriver());

        HashMap<String, Object> chromePreference = new HashMap<String, Object>();
        chromePreference.put("download.default_directory", FileUtils.getTempDirectoryPath());
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs", chromePreference);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    private void waitingElement(By locator) {
        waitForAjaxProcessed();
        waitForPresent(locator);
        highlight(locator);
    }

    public void open(String url) {
        Logger.info("Open URL: " + url);
        driver.get(url);
        takeScreenShot();
    }

    public void click(By locator) {
        Logger.info("Click on element: " + locator);
        waitingElement(locator);
        driver.findElement(locator).click();
        takeScreenShot();
    }

    public void twoClick(By locatorOne, By locatorTwo) {
        click(locatorOne);
        click(locatorTwo);
    }

    public void type(By locator, String text) {
        Logger.info("Type text: " + text + " on: " + locator);
        waitingElement(locator);
        driver.findElement(locator).sendKeys(text);
        takeScreenShot();
    }

    public void submit(By locator) {
        Logger.info("Submit on: " + locator);
        waitingElement(locator);
        driver.findElement(locator).submit();
        takeScreenShot();
    }

    public void clickAndWait(By locator) {
        Logger.info("Click and wait on: " + locator);
        waitingElement(locator);
        driver.findElement(locator).click();
        Timeout.wait(WAIT_FOR_DOWNLOAD_TIMEOUT);
        takeScreenShot();
    }

    public void uploadFile(By locator, String filePath) {
        Logger.info("Attach file: " + filePath + "on: " + locator);
        waitingElement(locator);
        driver.findElement(locator).sendKeys(filePath);
        Timeout.wait(WAIT_FOR_DOWNLOAD_TIMEOUT);
        takeScreenShot();
    }

    public void downloadFile(By locator) {
        Logger.info("Download file from: " + locator);
        waitingElement(locator);
        driver.findElement(locator).click();
        Timeout.wait(WAIT_FOR_DOWNLOAD_TIMEOUT);
        takeScreenShot();
    }

    public void dragAndDrop(By sourceLocator, By targetLocator) {
        Logger.info("Drag and drop: " + sourceLocator + " on: " + targetLocator);
        waitingElement(sourceLocator);
        WebElement source = driver.findElement(sourceLocator);
        waitingElement(targetLocator);
        WebElement target = driver.findElement(targetLocator);
        new Actions(driver).dragAndDrop(source, target).build().perform();
        Timeout.wait(WAIT_FOR_DOWNLOAD_TIMEOUT);
        takeScreenShot();
    }

    public void dragAndDropBy(By locator, int xOffset, int yOffset) {
        Logger.info("Drag and drop: " + locator + " on dom place: " + xOffset + "-" + yOffset);
        waitingElement(locator);
        WebElement draggable = driver.findElement(locator);
        new Actions(driver).dragAndDropBy(draggable, xOffset, yOffset).build().perform();
        takeScreenShot();
    }

    public void doubleClick(By locator) {
        Logger.info("Double click on: " + locator);
        waitingElement(locator);
        WebElement element = driver.findElement(locator);
        new Actions(driver).doubleClick(element).build().perform();
        takeScreenShot();
    }

    public void mouseOver(By locator) {
        Logger.info("Mouse over: " + locator);
        waitingElement(locator);
        WebElement element = driver.findElement(locator);
        new Actions(driver).moveToElement(element).build().perform();
        takeScreenShot();
    }

    public WebElement getElement(By locator) {
        Logger.info("Get element by: " + locator);
        waitingElement(locator);
        takeScreenShot();
        return driver.findElement(locator);
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return isElementPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info("Wait for visible: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info("Wait for Ajax requests");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public boolean isElementPresent(By locator) {
        Logger.info("Is element present: " + locator);
        Timeout.wait(1);
        return driver.findElements(locator).size() > 0;
    }

    public boolean isElementOnPage(By locator) {
        Logger.info("Is element present on page: " + locator);
        try {
            waitingElement(locator);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void takeScreenShot() {
        if ((Options.getInstance().getBrowserType() == BrowserType.FIREFOXR ||
                Options.getInstance().getBrowserType() == BrowserType.CHROMER) &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info("Screenshot saved to: " + String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot:");
        }
    }

    public void highlight(By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public synchronized void kill() {
        Logger.info("Close browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                //Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Browser can't kill!", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void close() {
        Logger.info("Close window");
        driver.close();
    }
}
