package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 19.03.2015.
 */
public class ComposeMailPage {

    private Element toInput = new Element(By.xpath("//div[contains(@class,'mail-input_to')]//input[@type = 'text']"));
    private Element subjectInput = new Element(By.xpath("//div[@class = 'b-input']/input[@name = 'subj']"));
    private Element contentInput = new Element(By.id("compose-send"));
    private Element attachInput = new Element(By.xpath("//input[@name='att']"));
    private Element sendEmailButton = new Element(By.id("nb-6"));
    private Element successSentLabel = new Element(By.xpath("//div[text()='Письмо успешно отправлено.']"));

    public MailboxBasePage sendEmail(String to, String subject, String content, String attach) {
        toInput.type(to);
        subjectInput.type(subject);
        contentInput.type(content);

        if (attach != null) {
            attachInput.uploadFile(attach);
        }

        sendEmailButton.click();
        successSentLabel.isElementOnPage();
        return new MailboxBasePage();
    }
}
