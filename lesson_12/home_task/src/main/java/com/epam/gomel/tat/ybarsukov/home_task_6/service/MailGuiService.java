package com.epam.gomel.tat.ybarsukov.home_task_6.service;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;
import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestEmailNotFoundException;
import com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail.*;
import com.epam.gomel.tat.ybarsukov.home_task_6.reporting.Logger;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class MailGuiService {

    public void sendEmail(Letter letter) {
        Logger.info("Send email to: " + letter.getReceiver());
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().openComposePage().sendEmail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
    }

    public void deleteEmail(Letter letter) {
        Logger.info("Delete email with subject: " + letter.getSubject());
        new InboxMailListPage().deleteEmail(letter.getSubject());
    }

    public void deleteEmailForever(Letter letter) {
        Logger.info("Delete email with subject: " + letter.getSubject());
        new DeletedMailListPage().deleteEmail(letter.getSubject());
    }

    public void markEmailAsSpam(Letter letter) {
        Logger.info("Mark email with subject: " + letter.getSubject() + " as a spam");
        new InboxMailListPage().markEmailAsSpam(letter.getSubject());
    }

    public void markEmailAsNotSpam(Letter letter) {
        Logger.info("Mark email with subject: " + letter.getSubject() + " as not spam");
        new SpamMailListPage().markEmailAsNotSpam(letter.getSubject());
    }

    public void downloadAttach() {
        Logger.info("Download attach");
        new LetterContentPage().downloadAttach();
    }

    public boolean isEmailInInboxList(Letter letter) {
        Logger.info("Find email with subject: " + letter.getSubject() + " in Inbox");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return mailbox.openInboxPage().isMessagePresent(letter.getSubject());
    }

    public boolean isEmailInSentList(Letter letter) {
        Logger.info("Find email with subject: " + letter.getSubject() + " in Sent");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return mailbox.openSentPage().isMessagePresent(letter.getSubject());
    }

    public boolean isEmailInDeletedList(Letter letter) {
        Logger.info("Find email with subject: " + letter.getSubject() + " in Deleted");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return mailbox.openDeletedPage().isMessagePresent(letter.getSubject());
    }

    public boolean isEmailInSpamList(Letter letter) {
        Logger.info("Find email with subject: " + letter.getSubject() + " in Spam");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return mailbox.openSpamPage().isMessagePresent(letter.getSubject());
    }

    public boolean isEmailInDraftsList(Letter letter) {
        Logger.info("Find email with subject: " + letter.getSubject() + " in Drafts");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return mailbox.openDraftsPage().isMessagePresent(letter.getSubject());
    }

    public boolean isEmailInInboxListValid(Letter letter) {
        Logger.info("Check email content with subject: " + letter.getSubject() + " in Inbox");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return isContentValid(mailbox.openInboxPage(), letter);
    }

    public boolean isEmailInSentListValid(Letter letter) {
        Logger.info("Check email content with subject: " + letter.getSubject() + " in Sent");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return isContentValid(mailbox.openSentPage(), letter);
    }

    public boolean isEmailInDeletedListValid(Letter letter) {
        Logger.info("Check email content with subject: " + letter.getSubject() + " in Deleted");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return isContentValid(mailbox.openDeletedPage(), letter);
    }

    public boolean isEmailInSpamListValid(Letter letter) {
        Logger.info("Check email content with subject: " + letter.getSubject() + " in Spam");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return isContentValid(mailbox.openSpamPage(), letter);
    }

    public boolean isEmailInDraftsListValid(Letter letter) {
        Logger.info("Check email content with subject: " + letter.getSubject() + " in Drafts");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return isContentValid(mailbox.openDraftsPage(), letter);
    }

    public boolean isEmailAbsentOnInboxList(Letter letter) {
        Logger.info("Check that email absent on inbox list");
        return new InboxMailListPage().isMessageAbsent(letter.getSubject());
    }

    public boolean isEmailAbsentOnSentList(Letter letter) {
        Logger.info("Check that email absent on sent list");
        return new SentMailListPage().isMessageAbsent(letter.getSubject());
    }

    public boolean isEmailAbsentOnSpamList(Letter letter) {
        Logger.info("Check that email absent on spam list");
        return new SpamMailListPage().isMessageAbsent(letter.getSubject());
    }

    public boolean isEmailAbsentOnDeletedList(Letter letter) {
        Logger.info("Check that email absent on deleted list");
        return new DeletedMailListPage().isMessageAbsent(letter.getSubject());
    }

    public boolean isEmailAbsentOnDraftsList(Letter letter) {
        Logger.info("Check that email absent on drafts list");
        return new DraftsMailListPage().isMessageAbsent(letter.getSubject());
    }

    private boolean isContentValid(AbstractMailListPage mailListPage, Letter letter) {
        Logger.info("Start checking");
        if (!mailListPage.isMessagePresent(letter.getSubject())) {
            throw new TestEmailNotFoundException("Letter with subject: " + letter.getSubject() + " does not exist!");
        }
        if (!mailListPage.openLetter(letter.getSubject()).isValidLetter(letter)) {
            throw new TestEmailNotFoundException("Letter with subject: " + letter.getSubject() + " and content: " + letter.getContent() + " does not exist!");
        }
        return true;
    }
}
