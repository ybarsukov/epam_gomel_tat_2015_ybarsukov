package com.epam.gomel.tat.ybarsukov.home_task_6.steps;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.Account;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.User;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.LoginGuiService;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.MailGuiService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

/**
 * Created by ybarsukov on 11.04.2015.
 */
public class SpamEmailSteps {

    private User user = new User();
    private Letter letter;
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();

    @Given("User has letter in inbox list")
    public void userHasLetterInInboxList() {
        Account account = user.createAccount(AccountBuilder.getDefaultAccount());
        loginGuiService.loginToAccountMailbox(account);
        letter = user.createLetter(LetterBuilder.getDefaultLetter());
        mailGuiService.sendEmail(letter);
        Assert.assertTrue(mailGuiService.isEmailInInboxList(letter));
    }

    @When("User mark letter as spam")
    public void userMarkLetterAsSpam() {
        mailGuiService.markEmailAsSpam(letter);
    }

    @Then("Letter appears in spam list")
    public void checkLetterInSpamList() {
        Assert.assertTrue(mailGuiService.isEmailInSpamList(letter));
    }

    @Given("User has letter in spam list")
    public void userHasLetterInSpamList() {
        userHasLetterInInboxList();
        userMarkLetterAsSpam();
        checkLetterInSpamList();
    }

    @When("User mark letter as not spam")
    public void userMarkLetterAsNotSpam() {
        mailGuiService.markEmailAsNotSpam(letter);
    }

    @Then("Letter appears in inbox list")
    public void checkLetterInInboxList() {
        Assert.assertTrue(mailGuiService.isEmailAbsentOnSpamList(letter) &&
                mailGuiService.isEmailInInboxList(letter));
    }
}
