package com.epam.gomel.tat.ybarsukov.home_task_6.utils;

import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestUnsupportedBrowserException;
import com.epam.gomel.tat.ybarsukov.home_task_6.global.BrowserType;
import com.epam.gomel.tat.ybarsukov.home_task_6.global.Options;
import com.epam.gomel.tat.ybarsukov.home_task_6.reporting.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by ybarsukov on 22.03.2015.
 */
public class FileTools extends FileUtils {

    public static File createFile(String randomText) {
        File file = getFile(getTempDirectoryPath() + randomText + ".txt");
        writeDataToFile(file);
        return file;
    }

    public static void writeDataToFile(File file) {
        try {
            write(file, RandomUtils.getRandomString("ATTACH_CONTENT_"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isAttachesEquals(String attachFilePath) {
        boolean isEquals = false;
        StringBuffer downloadFilePath = new StringBuffer(attachFilePath);

        BrowserType browserType = Options.getInstance().getBrowserType();
        switch (browserType) {
            case FIREFOX: {
                downloadFilePath.insert(downloadFilePath.length() - 4, "(1)");
                break;
            }
            case CHROME: {
                downloadFilePath.insert(downloadFilePath.length() - 4, " (1)");
                break;
            }
            default: {
                Logger.error(browserType + " Is unsupported for downloads browser!");
                throw new TestUnsupportedBrowserException(browserType + " Is unsupported for downloads browser!");
            }
        }
        try {
            isEquals = contentEquals(new File(attachFilePath), new File(downloadFilePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isEquals;
    }

    public static InputStream getResourceAsStream(String filePath) {
        return FileTools.class.getClassLoader().getResourceAsStream(filePath);
    }

    public static String getResource(String filePath) {
        return FileTools.class.getClassLoader().getResource(filePath).getPath();
    }

    public static String getFileName(String filePath) {
        return Paths.get(filePath).getFileName().toString();
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileTools.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        }

        return null;
    }
}
