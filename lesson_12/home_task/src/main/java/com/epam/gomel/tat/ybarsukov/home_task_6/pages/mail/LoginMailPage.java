package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 19.03.2015.
 */
public class LoginMailPage {

    private Element yandexMailUrl = new Element("http://mail.yandex.ru");
    private Element loginInput = new Element(By.name("login"));
    private Element passwordInput = new Element(By.name("passwd"));
    private Element submitButton = new Element(By.xpath("//span[contains(@class, 'auth-form')]/button"));

    public LoginMailPage open() {
        yandexMailUrl.open();
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        loginInput.type(login);
        passwordInput.type(password);
        submitButton.submit();
        return new MailboxBasePage();
    }

    public ErrorLoginMailPage failLogin(String login, String password) {
        loginInput.type(login);
        passwordInput.type(password);
        submitButton.submit();
        return new ErrorLoginMailPage();
    }
}
