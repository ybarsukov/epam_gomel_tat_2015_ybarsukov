package com.epam.gomel.tat.ybarsukov.home_task_6.steps;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.User;
import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestLoginException;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.LoginGuiService;
import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Browser;
import org.jbehave.core.annotations.*;
import org.testng.Assert;

/**
 * Created by ybarsukov on 09.04.2015.
 */
public class LoginToMailSteps {

    private User user;
    private LoginGuiService loginGuiService = new LoginGuiService();

    @Given("User is mailbox owner")
    public void userIsMailboxOwner() {
        user = new User();
    }

    @Given("User has correct login and correct password")
    public void userHasCorrectLoginAndPassword() {
        user.createAccount(AccountBuilder.getDefaultAccount());
    }

    @Given("User has correct login and incorrect password")
    public void userHasCorrectLoginAndIncorrectPassword() {
        user.createAccount(AccountBuilder.getAccountWithIncorrectPassword());
    }

    @When("User login to mailbox")
    public void userLoginToMailbox() {
        loginGuiService.loginToAccountMailbox(user.getAccount());
    }

    @Then("User has access to mail list")
    public void checkAccessToMailList() {
        try {
            Assert.assertTrue(loginGuiService.isMailListAvailable(user.getAccount()));
        } catch (TestLoginException e) {
            Assert.fail();
        }
    }

    @Then("User gets message about incorrect password")
    public void checkPasswordErrorMessage() {
        try {
            Assert.assertTrue(loginGuiService.isPasswordErrorMessage());
        } catch (TestLoginException e) {
            Assert.fail();
        }
    }

    @AfterScenario
    public void browserKill() {
        Browser.getInstance().kill();
    }
}
