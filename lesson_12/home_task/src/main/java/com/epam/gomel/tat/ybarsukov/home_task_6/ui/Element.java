package com.epam.gomel.tat.ybarsukov.home_task_6.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by ybarsukov on 07.04.2015.
 */
public class Element {

    private By locator;
    private String context;

    public Element(By locator) {
        this.locator = locator;
    }

    public Element(String context) {
        this.context = context;
    }

    public void setPattern(String pattern) {
        locator = By.xpath((String.format(context, pattern)));
    }

    public void open() {
        Browser.getInstance().open(context);
    }

    public void click() {
        Browser.getInstance().click(locator);
    }

    public void type(String text) {
        Browser.getInstance().type(locator, text);
    }

    public void submit() {
        Browser.getInstance().submit(locator);
    }

    public void uploadFile(String filePath) {
        Browser.getInstance().uploadFile(locator, filePath);
    }

    public void clickAndWait() {
        Browser.getInstance().clickAndWait(locator);
    }

    public void downloadFile() {
        Browser.getInstance().downloadFile(locator);
    }

    public void dragAndDrop(Element targetElement) {
        Browser.getInstance().dragAndDrop(locator, targetElement.getLocator());
    }

    public void doubleClick() {
        Browser.getInstance().doubleClick(locator);
    }

    public boolean isElementOnPage() {
        return Browser.getInstance().isElementOnPage(locator);
    }

    public boolean isElementPresent() {
        return Browser.getInstance().isElementPresent(locator);
    }

    public void twoClick(Element elementTwo) {
        Browser.getInstance().twoClick(locator, elementTwo.getLocator());
    }

    public WebElement getElement() {
        return Browser.getInstance().getElement(locator);
    }

    public By getLocator() {
        return locator;
    }

    public String getContext() {
        return context;
    }
}
