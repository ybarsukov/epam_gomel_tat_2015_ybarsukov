package com.epam.gomel.tat.ybarsukov.home_task_6.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class RandomUtils {

    public static String getRandomString(String text) {
        return text + RandomStringUtils.randomAlphanumeric(10);
    }

    public static String getRandomString() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

    public static String getRandomNumeric(String text) {
        return text + RandomStringUtils.randomNumeric(10);
    }

    public static String getRandomNumeric() {
        return RandomStringUtils.randomNumeric(10);
    }
}
