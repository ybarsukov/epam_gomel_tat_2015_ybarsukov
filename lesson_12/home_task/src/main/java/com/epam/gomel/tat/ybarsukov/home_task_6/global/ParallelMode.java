package com.epam.gomel.tat.ybarsukov.home_task_6.global;

/**
 * Created by ybarsukov on 25.03.2015.
 */
public enum ParallelMode {
    FALSE,
    TESTS,
    CLASSES,
    METHODS,
    SUITES
}
