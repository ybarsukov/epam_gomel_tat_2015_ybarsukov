package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 25.03.2015.
 */
public class TestUnsupportedBrowserException extends TestCommonRuntimeException{
    public TestUnsupportedBrowserException(String s) {
        super(s);
    }

    public TestUnsupportedBrowserException(String message, Throwable cause) {
        super(message, cause);
    }
}
