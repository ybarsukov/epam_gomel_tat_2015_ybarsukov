package com.epam.gomel.tat.ybarsukov.home_task_6.bo.common;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;

/**
 * Created by ybarsukov on 09.04.2015.
 */
public class User {
    private Account account;
    private Letter letter;

    public Account createAccount(Account account) {
        return this.account = account;
    }

    public Letter createLetter(Letter letter) {
        return this.letter = letter;
    }

    public Account getAccount() {
        return account;
    }

    public Letter getLetter() {
        return letter;
    }
}
