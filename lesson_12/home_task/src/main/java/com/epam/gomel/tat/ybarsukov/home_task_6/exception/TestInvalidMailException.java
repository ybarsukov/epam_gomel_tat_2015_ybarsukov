package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 01.04.2015.
 */
public class TestInvalidMailException extends TestCommonRuntimeException{
    public TestInvalidMailException(String message) {
        super(message);
    }

    public TestInvalidMailException(String message, Throwable cause) {
        super(message, cause);
    }
}
