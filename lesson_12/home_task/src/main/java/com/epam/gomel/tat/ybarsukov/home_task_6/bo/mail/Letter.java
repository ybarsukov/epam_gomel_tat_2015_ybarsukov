package com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class Letter {

    private String receiver;
    private String subject;
    private String content;
    private String attach;

    public Letter(String receiver, String subject, String content, String attach) {
        this.receiver = receiver;
        this.subject = subject;
        this.content = content;
        this.attach = attach;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "attach='" + attach + '\'' +
                ", receiver='" + receiver + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
