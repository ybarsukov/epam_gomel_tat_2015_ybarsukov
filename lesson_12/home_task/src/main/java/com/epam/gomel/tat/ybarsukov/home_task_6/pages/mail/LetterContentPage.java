package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;
import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class LetterContentPage {

    private Element subjectInput = new Element("//span[contains(@class,'message-subject')][text()='%s']");
    private Element receiverInput = new Element("//span[1][@class = 'b-message-head__email'][contains(.,'%s')]");
    private Element contentInput = new Element("//div[@class = 'block-message-body']//div[@class='b-message-body__content']/p[text()='%s']");
    private Element downloadButton = new Element(By.xpath("//span[1][@class = 'b-file__actions']/a[text() = 'Скачать']"));

    public boolean isValidLetter(Letter letter) {
        subjectInput.setPattern(letter.getSubject());
        receiverInput.setPattern(letter.getReceiver());
        contentInput.setPattern(letter.getContent());

        if (subjectInput.isElementOnPage()
                && receiverInput.isElementOnPage()
                && contentInput.isElementOnPage()
                ) {
            return true;
        }
        return false;
    }

    public void downloadAttach() {
        downloadButton.downloadFile();
    }
}
