package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 21.03.2015.
 */
public class TestLoginException extends TestCommonRuntimeException{
    public TestLoginException(String s) {
        super(s);
    }

    public TestLoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
