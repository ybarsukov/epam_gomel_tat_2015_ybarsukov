package com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.ui.Element;
import org.openqa.selenium.By;

/**
 * Created by ybarsukov on 19.03.2015.
 */
public class InboxMailListPage extends AbstractMailListPage {

    private Element composeButton = new Element(By.xpath("//a[@data-action='compose.go']"));
    private static Element emailLink = new Element("//span[@class = 'js-messages-title-dropdown-name']/ancestor::div[@class = 'block-messages']//span[@class = 'b-messages__subject'][text()='%s']");
    private static Element emailCheckbox = new Element("//span[@class = 'js-messages-title-dropdown-name']/ancestor::div[@class = 'block-messages']//span[@class = 'b-messages__subject'][text()='%s']/ancestor::div[1]//input[@type = 'checkbox']");

    public InboxMailListPage() {
        super(emailLink, emailCheckbox);
    }

    public ComposeMailPage openComposePage() {
        composeButton.click();
        return new ComposeMailPage();
    }
}
