package com.epam.gomel.tat.ybarsukov.home_task_6.steps;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.Account;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.User;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.Letter;
import com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.LoginGuiService;
import com.epam.gomel.tat.ybarsukov.home_task_6.service.MailGuiService;
import com.epam.gomel.tat.ybarsukov.home_task_6.utils.FileTools;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

/**
 * Created by ybarsukov on 10.04.2015.
 */
public class SendEmailSteps {

    private User user = new User();
    private Letter letter;
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();

    @Given("User has access to mail list")
    public void userHasAccessToMailList() {
        Account account = user.createAccount(AccountBuilder.getDefaultAccount());
        loginGuiService.loginToAccountMailbox(account);
    }

    @When("User compose and sent letter without attach")
    public void userComposeAndSentLetterWithoutAttach() {
        letter = user.createLetter(LetterBuilder.getDefaultLetter());
        mailGuiService.sendEmail(letter);
    }

    @When("User compose and sent letter with attach")
    public void userComposeAndSentLetterWithAttach() {
        letter = user.createLetter(LetterBuilder.getLetterWithAttach());
        mailGuiService.sendEmail(letter);
    }

    @Then("Letter appears in sent list")
    public void checkLetterAppearsInSentList() {
        Assert.assertTrue(mailGuiService.isEmailInSentList(letter));
    }

    @Then("Sended letter content equals composed letter content")
    public void checkSendedLetterEqualsComposedLetter() {
        Assert.assertTrue(mailGuiService.isEmailInSentListValid(letter));
    }

    @Then("Attached file is equals downloaded file")
    public void checkAttachedAndDownloadedFiles() {
        mailGuiService.downloadAttach();
        Assert.assertTrue(FileTools.isAttachesEquals(letter.getAttach()));
    }
}
