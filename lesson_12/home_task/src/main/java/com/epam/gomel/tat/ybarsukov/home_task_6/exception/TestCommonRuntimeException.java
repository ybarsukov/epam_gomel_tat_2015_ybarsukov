package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class TestCommonRuntimeException extends RuntimeException {
    public TestCommonRuntimeException(String s) {
        super(s);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
