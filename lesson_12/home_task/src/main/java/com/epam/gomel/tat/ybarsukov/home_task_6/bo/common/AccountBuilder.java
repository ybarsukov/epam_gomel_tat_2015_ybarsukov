package com.epam.gomel.tat.ybarsukov.home_task_6.bo.common;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class AccountBuilder {

    public static Account getDefaultAccount() {
        return new Account("ybarsukov.tat", "qwerty_123", "ybarsukov.tat@yandex.ru");
    }

    public static Account getAccountWithIncorrectPassword() {
        Account account = getDefaultAccount();
        account.setPassword("123");
        return account;
    }
}
