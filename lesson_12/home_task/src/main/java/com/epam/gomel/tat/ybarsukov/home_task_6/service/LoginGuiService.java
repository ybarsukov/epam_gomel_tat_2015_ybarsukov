package com.epam.gomel.tat.ybarsukov.home_task_6.service;

import com.epam.gomel.tat.ybarsukov.home_task_6.bo.common.Account;
import com.epam.gomel.tat.ybarsukov.home_task_6.exception.TestLoginException;
import com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail.ErrorLoginMailPage;
import com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail.LoginMailPage;
import com.epam.gomel.tat.ybarsukov.home_task_6.pages.mail.MailboxBasePage;
import com.epam.gomel.tat.ybarsukov.home_task_6.reporting.Logger;

/**
 * Created by ybarsukov on 20.03.2015.
 */
public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.info("Login to account " + account);
        new LoginMailPage().open().login(account.getLogin(), account.getPassword());
    }

    public boolean isMailListAvailable(Account account) {
        Logger.info("Is mail list available for account: " + account);
        String userEmail = new MailboxBasePage().getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            throw new TestLoginException("Login failed. User Mail : " + account.getEmail());
        }
        return true;
    }

    public boolean isPasswordErrorMessage() {
        Logger.info("Is password error message valid");
        String errorMessage = new ErrorLoginMailPage().getErrorMessage();
        if (errorMessage == null || !errorMessage.equals(ErrorLoginMailPage.INCORRECT_PASSWORD_MESSAGE)) {
            throw new TestLoginException("This is not password error message: " + errorMessage);
        }
        return true;
    }
}
