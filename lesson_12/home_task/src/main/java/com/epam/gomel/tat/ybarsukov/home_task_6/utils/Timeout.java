package com.epam.gomel.tat.ybarsukov.home_task_6.utils;

/**
 * Created by ybarsukov on 31.03.2015.
 */
public class Timeout {

    public static void wait(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
