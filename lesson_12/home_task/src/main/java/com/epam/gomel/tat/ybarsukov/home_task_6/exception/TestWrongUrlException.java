package com.epam.gomel.tat.ybarsukov.home_task_6.exception;

/**
 * Created by ybarsukov on 26.03.2015.
 */
public class TestWrongUrlException extends TestCommonRuntimeException {
    public TestWrongUrlException(String s) {
        super(s);
    }

    public TestWrongUrlException(String message, Throwable cause) {
        super(message, cause);
    }
}
