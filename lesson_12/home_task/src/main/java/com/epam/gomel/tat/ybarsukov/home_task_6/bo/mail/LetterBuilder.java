package com.epam.gomel.tat.ybarsukov.home_task_6.bo.mail;

import com.epam.gomel.tat.ybarsukov.home_task_6.utils.FileTools;
import com.epam.gomel.tat.ybarsukov.home_task_6.utils.RandomUtils;

/**
 * Created by ybarsukov on 21.03.2015.
 */
public class LetterBuilder {

    public static Letter getDefaultLetter() {
        return new Letter("ybarsukov.tat@yandex.ru", (RandomUtils.getRandomNumeric("SUBJECT_")), (RandomUtils.getRandomString()), null);
    }

    public static Letter getLetterWithAttach() {
        Letter letter = getDefaultLetter();
        letter.setAttach((FileTools.createFile(RandomUtils.getRandomString("ATTACH_")).getAbsolutePath()));
        return letter;
    }
}
