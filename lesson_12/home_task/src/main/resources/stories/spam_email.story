Meta: Yandex mail spam story

Scenario: mark email as a spam
Given User has letter in inbox list
When User mark letter as spam
Then Letter appears in spam list

Scenario: mark email as not spam
Given User has letter in spam list
When User mark letter as not spam
Then Letter appears in inbox list