Meta: Yandex mail login story

Scenario: Success login as mailbox owner
Given User is mailbox owner
And User has correct login and correct password
When User login to mailbox
Then User has access to mail list

Scenario: Unsuccess login as mailbox owner
Given User is mailbox owner
And User has correct login and incorrect password
When User login to mailbox
Then User gets message about incorrect password
