Meta: Yandex mail send story

Scenario: Send email without attach to own account
Given User has access to mail list
When User compose and sent letter without attach
Then Letter appears in sent list
And Sended letter content equals composed letter content

Scenario: Send email with attach to own account
Given User has access to mail list
When User compose and sent letter with attach
Then Letter appears in sent list
And Sended letter content equals composed letter content
And Attached file is equals downloaded file

