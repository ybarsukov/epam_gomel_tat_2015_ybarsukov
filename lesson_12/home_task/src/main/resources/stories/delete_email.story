Meta: Yandex mail delete story

Scenario: delete email from inbox list
Given User has the letter in inbox list
When User mark letter as deleted
Then Letter appears in deleted list

Scenario: delete email forever
Given User has letter in deleted list
When User mark letter as deleted forever
Then Letter has deleted forever