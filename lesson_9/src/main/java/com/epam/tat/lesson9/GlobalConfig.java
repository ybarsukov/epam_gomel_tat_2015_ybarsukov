package com.epam.tat.lesson9;

import com.epam.tat.lesson9.browser.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.List;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class GlobalConfig {

    private static GlobalConfig instance;

    public static GlobalConfig instance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    @Option(name = "-bt", usage = "browser type", required = true)
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", usage = "thread count")
    private int threadCount = 1;

    @Option(name = "-suites", handler = StringArrayOptionHandler.class)
    private List<String> suites;

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }
}
