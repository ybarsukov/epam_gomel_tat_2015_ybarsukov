package com.epam.tat.lesson9.listener;

import com.epam.tat.lesson9.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by Aleh_Vasilyeu on 4/1/2015.
 */
public class MySuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.instance().getParallelMode().getAlias());
        suite.getXmlSuite().setThreadCount(GlobalConfig.instance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
