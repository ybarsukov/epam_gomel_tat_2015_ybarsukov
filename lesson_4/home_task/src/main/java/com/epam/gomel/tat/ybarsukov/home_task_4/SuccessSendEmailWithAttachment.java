package com.epam.gomel.tat.ybarsukov.home_task_4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**
 * Test Case : Success send email with attachment
 * Steps :
 * 1. Open account mail inbox
 * 2. Click send mail
 * 3. Fill inputs : TO, SUBJECT, MAIL CONTENT
 * 4. Send the mail
 * 5. Open mail sended messages list
 * 6. Open the sent mail
 * 7. Download attached file
 * 8. Check content of attached file - content should be the same
 */

public class SuccessSendEmailWithAttachment {

    // UI data
    public static final By LOGIN_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'login-button')]/a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By SUBMIT_INPUT_LOCATOR = By.className("b-mail-button__button");
    public static final By VALIDATE_MAIL_LOCATOR = By.xpath("//a[@id='nb-1']/span[1]");
    public static final By WRITE_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.go']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//div[contains(@class,'mail-input_to')]//input[@type = 'text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.xpath("//div[@class = 'b-input']/input[@name = 'subj']");
    public static final By MAIL_TEXT_INPUT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("nb-6");
    public static final By ATTACH_BUTTON_LOCATOR = By.name("att");
    public static final By SENT_BUTTON_LOCATOR = By.xpath("//a[@href = '#sent']");
    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//span[1][@class = 'b-file__actions']/a[2]");
    public static final String MAIL_LINK_LOCATOR_PATTERN = ("//span[@class = 'b-messages__subject'][text()='%s']");
    public static final String SUBJECT_INPUT_TEXT = "SOME_SUBJECT " + Property.getRandom();
    public static final String MAIL_CONTENT_AREA_TEXT = "SOME_TEXT " + Property.getRandom();

    // Web Driver
    private WebDriver driver;
    private FirefoxProfile profile;

    @BeforeClass(description = "Prepare Firefox browser")
    public void prepareFirefoxBrowser() {
        profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", "d:\\Epam\\epam_gomel_tat_2015_ybarsukov\\lesson_4\\home_task\\src\\main\\resources\\download\\");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(Integer.valueOf(Property.getValue(Property.PAGE_LOAD_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(Integer.valueOf(Property.getValue(Property.IMPLICITLY_WAIT_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void loginMail() {
        driver.get(Property.YANDEX_RU_URL);
        WebElement loginButton = driver.findElement(LOGIN_BUTTON_LOCATOR);
        loginButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(Property.USER_LOGIN);
        WebElement passwordInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwordInput.sendKeys(Property.USER_PASSWORD);
        WebElement submitInput = driver.findElement(SUBMIT_INPUT_LOCATOR);
        submitInput.click();
        WebElement validateLink = driver.findElement(VALIDATE_MAIL_LOCATOR);
        Assert.assertEquals(validateLink.isEnabled(), true, Property.MESSAGE_THAT_IS_TEST_FAIL);
    }

    @Test(description = "Send mail with Attach", dependsOnMethods = "loginMail")
    public void sendMailWithAttach() {
        WebElement writeButton = driver.findElement(WRITE_BUTTON_LOCATOR);
        writeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(Property.TEST_EMAIL);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(SUBJECT_INPUT_TEXT);
        WebElement mailTextArea = driver.findElement(MAIL_TEXT_INPUT_LOCATOR);
        mailTextArea.sendKeys(MAIL_CONTENT_AREA_TEXT);
        WebElement attachButton = driver.findElement(ATTACH_BUTTON_LOCATOR);
        attachButton.click();

        driver.switchTo().activeElement().sendKeys(Property.UPLOAD_ATTACH_FILE_PATH); //TODO Close active window
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    @Test(description = "Check sent mail", dependsOnMethods = "sendMailWithAttach")
    public void checkSentMail() {
        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(SENT_BUTTON_LOCATOR));
        WebElement sentButton = driver.findElement(SENT_BUTTON_LOCATOR);
        sentButton.click();

        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, SUBJECT_INPUT_TEXT))));
        WebElement mailLink = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, SUBJECT_INPUT_TEXT)));
        mailLink.click();
    }

    @Test(description = "Download attached file", dependsOnMethods = "checkSentMail")
    public void downloadAttach() {
        WebElement downloadButton = driver.findElement(DOWNLOAD_BUTTON_LOCATOR);
        downloadButton.click();

        driver.switchTo().activeElement().sendKeys(Property.DOWNLOAD_ATTACH_FILE_PATH); //TODO Close active window
    }

    @Test(description = "Check equals upload/download data", dependsOnMethods = "downloadAttach")
    public void checkEquals() {
        Assert.assertEquals(Property.isDataEquals(Property.UPLOAD_ATTACH_FILE_PATH, Property.DOWNLOAD_ATTACH_FILE_PATH), true, Property.MESSAGE_THAT_IS_TEST_FAIL);
    }

    @AfterClass(description = "Close browser")
    public void quitBrowser() {
        driver.quit();
    }
}
