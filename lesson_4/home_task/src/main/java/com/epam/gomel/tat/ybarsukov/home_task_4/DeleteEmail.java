package com.epam.gomel.tat.ybarsukov.home_task_4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**
Test Case : Delete email
Precondition : There is a new email message in account inbox
Steps :
1. Open account mail inbox
2. Delete the message
3. Open deleted messages list
4. The deleted message should be in list
 */

public class DeleteEmail {

    // UI data
    public static final By LOGIN_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'login-button')]/a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By SUBMIT_INPUT_LOCATOR = By.className("b-mail-button__button");
    public static final By VALIDATE_MAIL_LOCATOR = By.xpath("//a[@id='nb-1']/span[1]");
    public static final By WRITE_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.go']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//div[contains(@class,'mail-input_to')]//input[@type = 'text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.xpath("//div[@class = 'b-input']/input[@name = 'subj']");
    public static final By MAIL_TEXT_INPUT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("nb-6");
    public static final By INBOX_BUTTON_LOCATOR = By.xpath("//div[@class = 'b-folders__i']//a[@href = '#inbox']");
    public static final By DELETE_MAIL_BUTTON = By.xpath("//div[@class = 'b-toolbar__i']//a[@data-action = 'delete']");
    public static final By TRASH_BUTTON_LOCATOR = By.xpath("//div[@class = 'b-folders__i']//a[@href = '#trash']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = ("//span[@class = 'b-messages__subject'][text()='%s']");
    public static final String CHECK_BOX_LOCATOR_PATTERN = ("//span[@class = 'b-messages__subject'][text()='%s']/ancestor::div[1]//input[@type = 'checkbox']");
    public static final String SUBJECT_INPUT_TEXT = "SOME_SUBJECT " + Property.getRandom();
    public static final String MAIL_CONTENT_AREA_TEXT = "SOME_TEXT " + Property.getRandom();

    // Web Driver
    private WebDriver driver;

    @BeforeClass(description = "Prepare Firefox browser")
    public void prepareFirefoxBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(Integer.valueOf(Property.getValue(Property.PAGE_LOAD_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(Integer.valueOf(Property.getValue(Property.IMPLICITLY_WAIT_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void loginMail() {
        driver.get(Property.YANDEX_RU_URL);
        WebElement loginButton = driver.findElement(LOGIN_BUTTON_LOCATOR);
        loginButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(Property.USER_LOGIN);
        WebElement passwordInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwordInput.sendKeys(Property.USER_PASSWORD);
        WebElement submitInput = driver.findElement(SUBMIT_INPUT_LOCATOR);
        submitInput.click();
        WebElement validateLink = driver.findElement(VALIDATE_MAIL_LOCATOR);
        Assert.assertEquals(validateLink.isEnabled(), true, Property.MESSAGE_THAT_IS_TEST_FAIL);
    }

    @Test(description = "Send mail", dependsOnMethods = "loginMail")
    public void sendMail() {
        WebElement writeButton = driver.findElement(WRITE_BUTTON_LOCATOR);
        writeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(Property.TEST_EMAIL);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(SUBJECT_INPUT_TEXT);
        WebElement mailTextArea = driver.findElement(MAIL_TEXT_INPUT_LOCATOR);
        mailTextArea.sendKeys(MAIL_CONTENT_AREA_TEXT);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    @Test(description = "Find mail", dependsOnMethods = "sendMail")
    public void findMail() {
        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(INBOX_BUTTON_LOCATOR));
        WebElement sentButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        sentButton.click();

        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, SUBJECT_INPUT_TEXT))));
    }

    @Test(description = "Delete mail", dependsOnMethods = "findMail")
    public void deleteMail() {
        WebElement checkBoxInput = driver.findElement(By.xpath(String.format(CHECK_BOX_LOCATOR_PATTERN, SUBJECT_INPUT_TEXT)));
        checkBoxInput.click();
        WebElement deleteButton = driver.findElement(DELETE_MAIL_BUTTON);
        deleteButton.click();
    }

    @Test(description = "Check deleted mail", dependsOnMethods = "deleteMail")
    public void checkDeletedMail(){
        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(TRASH_BUTTON_LOCATOR));
        WebElement trashButton = driver.findElement(TRASH_BUTTON_LOCATOR);
        trashButton.click();

        new WebDriverWait(driver, Integer.valueOf(Property.getValue(Property.TIME_OUT_MAIL_ARRIVED_SECONDS))).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, SUBJECT_INPUT_TEXT))));
    }

    @AfterClass(description = "Close browser")
    public void quitBrowser() {
        driver.quit();
    }
}


