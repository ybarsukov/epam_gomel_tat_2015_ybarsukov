package com.epam.gomel.tat.ybarsukov.home_task_4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**
 Test Case : Success login to yandex mail
 Steps :
 1. Open http://www.yandex.ru/
 2. Click button "Войти"
 3. Login input - type correct login name
 4. Password input - type correct password
 5. Click button "Войти"
 6. Validate success login (mail account link should be on the top-right of page)
 */

public class SuccessLoginToYandexMail {

    // UI data
    public static final By LOGIN_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'login-button')]/a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By SUBMIT_INPUT_LOCATOR = By.className("b-mail-button__button");
    public static final By VALIDATE_MAIL_LOCATOR = By.xpath("//a[@id='nb-1']/span[1]");

    // Web Driver
    private WebDriver driver;

    @BeforeClass(description = "Prepare Firefox browser")
    public void prepareFirefoxBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(Integer.valueOf(Property.getValue(Property.PAGE_LOAD_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(Integer.valueOf(Property.getValue(Property.IMPLICITLY_WAIT_TIMEOUT_SECONDS)), TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void loginMail() {
        driver.get(Property.YANDEX_RU_URL);
        WebElement loginButton = driver.findElement(LOGIN_BUTTON_LOCATOR);
        loginButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(Property.USER_LOGIN);
        WebElement passwordInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwordInput.sendKeys(Property.USER_PASSWORD);
        WebElement submitInput = driver.findElement(SUBMIT_INPUT_LOCATOR);
        submitInput.click();
        WebElement validateLink = driver.findElement(VALIDATE_MAIL_LOCATOR);
        Assert.assertEquals(validateLink.isEnabled(), true, Property.MESSAGE_THAT_IS_TEST_FAIL);
    }

    @AfterClass(description = "Close browser")
    public void quitBrowser() {
        driver.quit();
    }
}


