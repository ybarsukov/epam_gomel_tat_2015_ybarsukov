package com.epam.gomel.tat.ybarsukov.home_task_4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Created by ybarsukov on 14.03.2015.
 */
public final class Property {

    // AUT data
    protected static final String YANDEX_RU_URL = "http://yandex.ru";

    // User data
    protected static final String USER_LOGIN = "ybarsukov.tat";
    protected static final String USER_PASSWORD = "qwerty_123";
    protected static final String USER_INCORRECT_PASSWORD = "123";
    protected static final String EMAIL_TO_SEND = "admin@belstekloprom.com";
    protected static final String TEST_EMAIL = "ybarsukov.tat@yandex.by";

    // Message data
    protected static final String MESSAGE_THAT_IS_TEST_FAIL = "Test Failed!";
    protected static final String MESSAGE_THAT_IS_WRONG_PASSWORD = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    // Tools data
    protected static final String PAGE_LOAD_TIMEOUT_SECONDS = "PAGE_LOAD_TIMEOUT_SECONDS";
    protected static final String IMPLICITLY_WAIT_TIMEOUT_SECONDS = "IMPLICITLY_WAIT_TIMEOUT_SECONDS";
    protected static final String TIME_OUT_MAIL_ARRIVED_SECONDS = "TIME_OUT_MAIL_ARRIVED_SECONDS";

    // Data files
    protected static final String TOOLS_FILE_PATH = "src/main/resources/tools.property";
    protected static final String UPLOAD_ATTACH_FILE_PATH = "d:\\Epam\\epam_gomel_tat_2015_ybarsukov\\lesson_4\\home_task\\src\\main\\resources\\upload\\attach.txt";
    protected static final String DOWNLOAD_ATTACH_FILE_PATH = "d:\\Epam\\epam_gomel_tat_2015_ybarsukov\\lesson_4\\home_task\\src\\main\\resources\\download\\attach.txt";

    // Properties
    private static Properties properties = new Properties();

    // Метод принимает в качестве аргумента имя параметра property.
    // Возвращает из локального файла .property значение параметра переданного в аргументе
    protected static String getValue(String paramName) {
        String value;
        InputStream stream;
        InputStreamReader reader = null;

        try {
            stream = new FileInputStream(Property.TOOLS_FILE_PATH);
            reader = new InputStreamReader(stream);
            properties.load(reader);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        value = properties.getProperty(paramName);
        return value;
    }

    // Метод генерирует случайное число в диапазоне от min до max
    // Возвращает число
    protected static int getRandom() {
        int min = 0;
        int max = 1000000;
        int random;
        random = min + (int) (Math.random() * ((max - min) + 1));
        return random;
    }

    // Метод проверяет есть ли у драйвера элемент
    // Если получаем Исключение NoSuchElementException возвращаем false
    protected static boolean isElementPresent(WebDriver driver, By elementLocator) {
        try {
            driver.findElement(elementLocator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    // Метод сравнивает массы байтов из входящего потока двух файлов
    // В случае их соответствия возращает true
    protected static boolean isDataEquals(String filePathOne, String filePathTwo) {
        boolean isDataEquals = false;
        FileInputStream reader = null;

        try {
            reader = new FileInputStream(filePathOne);
            byte[] dataFromFileOne = new byte[reader.available()];
            reader.read(dataFromFileOne);
            reader.close();

            reader = new FileInputStream(filePathTwo);
            byte[] dataFromFileTwo = new byte[reader.available()];
            reader.read(dataFromFileTwo);
            reader.close();

            if (Arrays.equals(dataFromFileOne, dataFromFileTwo)) {
                isDataEquals = true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return isDataEquals;
        }
    }
}
